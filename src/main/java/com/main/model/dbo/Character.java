package com.main.model.dbo;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;


@Entity
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false, length = 40)
    public String name;

    @Column(length = 40)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String alias;

    @Column(nullable = false, length = 10)
    public String gender;

    @Column(length = 100)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String pictureURL;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "movie_characters",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    public List<Movie> movies;


    @JsonGetter("movies")
    public List<String> getMovieList(){
        return movies.stream()
                .map(movie -> {
                    return "/movie/" + movie.id;
                }).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object other){
        if(!(other instanceof Character)){
            return false;
        }else{
            return this.id == ((Character) other).id;
        }
    }

    @Override
    public int hashCode(){
        return this.name.hashCode();
    }
}
