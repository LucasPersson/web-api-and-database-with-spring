package com.main.model.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false, length = 40)
    public String name;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String description;

    @OneToMany(mappedBy = "franchise", fetch = FetchType.LAZY)
    public List<Movie> movies;


    @JsonGetter("movies")
    public List<String> getMovieList(){
        return movies.stream()
                .map(movie -> {
                    return "/movie/" + movie.id;
                }).collect(Collectors.toList());
    }

}
