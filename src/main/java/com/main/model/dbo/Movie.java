package com.main.model.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(nullable = false, length = 50)
    public String title;

    @Column(nullable = false, length = 100)
    public String genre;

    @Column(nullable = false)
    public int releaseYear;

    @Column(nullable = false, length = 40)
    public String director;

    @Column(length = 100)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String pictureLink;

    @Column(length = 100)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String trailerLink;

    @ManyToMany(mappedBy = "movies", fetch = FetchType.LAZY)
    public List<Character> characters;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Franchise franchise;

    @JsonGetter("franchise")
    public String getFranchise()
    {
        if(franchise != null)
        {
            return "/franchise/" + franchise.id;
        }
        else
        {
            return null;
        }
    }

    @JsonGetter("characters")
    public List<String> getCharacterList(){
        return characters.stream()
                .map(character -> {
                    return "/character/" + character.id;
                }).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object other){
        if(!(other instanceof Movie)){
            return false;
        }else{
            return this.id == ((Movie) other).id;
        }
    }

    @Override
    public int hashCode(){
        return this.title.hashCode();
    }
}
