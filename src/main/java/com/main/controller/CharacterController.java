package com.main.controller;

import com.main.model.dbo.Character;
import com.main.model.CommonResponse;
import com.main.model.dbo.Movie;
import com.main.repository.CharacterRepository;
import com.main.repository.FranchiseRepository;
import com.main.repository.MovieRepository;
import com.main.util.Command;
import com.main.util.Logger;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Character")
public class CharacterController {

    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;
    private final MovieController movieController;

    public CharacterController(CharacterRepository characterRepository, MovieRepository movieRepository, FranchiseRepository franchiseRepository, MovieController movieController) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
        this.movieController = movieController;
    }

    // Create a character
    @PostMapping("/character")
    public ResponseEntity<CommonResponse> createCharacter(HttpServletRequest request, @RequestBody Character character){
        Command cmd = new Command(request);

        character = characterRepository.save(character);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = character;
        commonResponse.message = "New character with id: " + character.id;

        HttpStatus response = HttpStatus.CREATED;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read a character by ID
    @GetMapping("/character/{id}")
    public ResponseEntity<CommonResponse> getCharacterById(HttpServletRequest request, @PathVariable Long id){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(characterRepository.existsById(id)) {
            commonResponse.data = characterRepository.findById(id);
            commonResponse.message = "Character ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Character does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read all characters
    @GetMapping("/character/all")
    public ResponseEntity<CommonResponse> getAllCharacters(HttpServletRequest request){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = characterRepository.findAll();
        commonResponse.message = "All characters";

        HttpStatus response = HttpStatus.OK;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Update a character by ID
    @PatchMapping("/character/{id}")
    public ResponseEntity<CommonResponse> updateCharacter(HttpServletRequest request, @RequestBody Character updateCharacter, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(characterRepository.existsById(id)) {
            Optional<Character> characterRepositoryById = characterRepository.findById(id);
            Character character = characterRepositoryById.get();

            if(updateCharacter.name != null) {
                character.name = updateCharacter.name;
            }
            if(updateCharacter.alias != null) {
                character.alias = updateCharacter.alias;
            }
            if(updateCharacter.gender != null) {
                character.gender = updateCharacter.gender;
            }
            if(updateCharacter.pictureURL != null) {
                character.pictureURL = updateCharacter.pictureURL;
            }

            characterRepository.save(character);

            commonResponse.data = character;
            commonResponse.message = "Updated character with ID: " + character.id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Character with ID: " + id + " does not exist.";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Delete a character
    @DeleteMapping("/character/{id}")
    public ResponseEntity<CommonResponse> deleteCharacter(HttpServletRequest request, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(characterRepository.existsById(id)) {
            Optional<Character> character = characterRepository.findById(id);
            Character theCharacter = character.get();
            for(Movie movie : theCharacter.movies){
                movie.characters.remove(theCharacter);
            }
            characterRepository.deleteById(id);
            commonResponse.message = "Deleted character with ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Character with ID: " + id + " does not exist.";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // get characters in a movie
    @GetMapping("/character/movie/{id}")
    public ResponseEntity<CommonResponse> getCharacterInMovie(HttpServletRequest request, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(movieRepository.existsById(id)) {

            Optional <Movie> movie = movieRepository.findById(id);

            Movie movieBody = movie.get();
            List<Character> charactersList = new ArrayList<>(movieBody.characters);
            commonResponse.data = charactersList;
            commonResponse.message = "Characters for movie with ID: " + id;

            response = HttpStatus.OK;

        } else {
            commonResponse.data = null;
            commonResponse.message = "Movie does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // get characters in a franchise
    @GetMapping("/character/franchise/{id}")
    public ResponseEntity<CommonResponse> getCharacterInFranchise(HttpServletRequest request, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(franchiseRepository.existsById(id)) {

            ResponseEntity<CommonResponse> movies = movieController.getMoviesInFranchise(request, id);
            CommonResponse movieResponse = movies.getBody();
            List<Movie> movieList = (List<Movie>) movieResponse.data;

            List<Character> charactersList = new ArrayList<>();

            for ( Movie movie : movieList) {

                for ( Character character : movie.characters) {

                    if (!charactersList.contains(character)) {

                        charactersList.add(character);
                    }
                }
            }

            commonResponse.data = charactersList;
            commonResponse.message = "Characters for franchise with ID: " + id;

            response = HttpStatus.OK;

        } else {
            commonResponse.data = null;
            commonResponse.message = "Franchise does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

}
