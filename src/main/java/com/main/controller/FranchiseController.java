package com.main.controller;

import com.main.model.CommonResponse;
import com.main.model.dbo.Character;
import com.main.model.dbo.Franchise;
import com.main.model.dbo.Movie;
import com.main.repository.FranchiseRepository;
import com.main.repository.MovieRepository;
import com.main.util.Command;
import com.main.util.Logger;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Franchise")
public class FranchiseController {

    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseController(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    // Create a franchise
    @PostMapping("/franchise")
    public ResponseEntity<CommonResponse> createFranchise(HttpServletRequest request, @RequestBody Franchise franchise){
        Command cmd = new Command(request);

        franchise = franchiseRepository.save(franchise);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = franchise;
        commonResponse.message = "New franchise with id: " + franchise.id;

        HttpStatus response = HttpStatus.CREATED;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read a franchise by ID
    @GetMapping("/franchise/{id}")
    public ResponseEntity<CommonResponse> getFranchiseById(HttpServletRequest request, @PathVariable Long id){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(franchiseRepository.existsById(id)) {
            commonResponse.data = franchiseRepository.findById(id);
            commonResponse.message = "Franchise ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Franchise does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read all franchises
    @GetMapping("/franchise/all")
    public ResponseEntity<CommonResponse> getAllFranchises(HttpServletRequest request){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = franchiseRepository.findAll();
        commonResponse.message = "All franchises";

        HttpStatus response = HttpStatus.OK;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Update a franchise by ID
    @PatchMapping("/franchise/{id}")
    public ResponseEntity<CommonResponse> updateFranchise(HttpServletRequest request, @RequestBody Franchise updateFranchise, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(franchiseRepository.existsById(id)) {
            Optional<Franchise> franchiseRepositoryById = franchiseRepository.findById(id);
            Franchise franchise = franchiseRepositoryById.get();

            if(updateFranchise.name != null) {
                franchise.name = updateFranchise.name;
            }
            if(updateFranchise.description != null) {
                franchise.description = updateFranchise.description;
            }

            franchiseRepository.save(franchise);

            commonResponse.data = franchise;
            commonResponse.message = "Updated franchise with ID: " + franchise.id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Franchise with ID: " + id + " does not exist.";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Delete a franchise
    @DeleteMapping("/franchise/{id}")
    public ResponseEntity<CommonResponse> deleteFranchise(HttpServletRequest request, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(franchiseRepository.existsById(id)) {
            Optional<Franchise> franchise = franchiseRepository.findById(id);
            Franchise theFranchise = franchise.get();
            for(Movie movie : theFranchise.movies){
                movie.franchise = null;
            }
            franchiseRepository.deleteById(id);
            commonResponse.message = "Deleted franchise with ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Franchise with ID: " + id + " does not exist.";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Replace list of characters in movie
    @PutMapping("/franchise/{id}")
    public ResponseEntity<CommonResponse> replaceMovie(HttpServletRequest request, @RequestBody List<Long> movies, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(franchiseRepository.existsById(id)) {
            Optional<Franchise> franchiseRepositoryById = franchiseRepository.findById(id);
            Franchise franchise = franchiseRepositoryById.get();

            franchise.movies = new ArrayList<>();

            for(Long movie_id : movies) {
                if(movieRepository.existsById(movie_id)){
                    Optional<Movie> movieOptional = movieRepository.findById(movie_id);
                    franchise.movies.add(movieOptional.get());
                }
            }

            Franchise newFranchise = franchiseRepository.save(franchise);
            commonResponse.data = newFranchise;
            commonResponse.message = "Replaced franchise with ID: " + newFranchise.id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Franchise not found with ID: " + id;
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }
}
