package com.main.controller;

import com.main.model.CommonResponse;
import com.main.model.dbo.Franchise;
import com.main.model.dbo.Movie;
import com.main.model.dbo.Character;
import com.main.repository.CharacterRepository;
import com.main.repository.MovieRepository;
import com.main.repository.FranchiseRepository;
import com.main.util.Command;
import com.main.util.Logger;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Movies")
public class MovieController {

    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;
    private final CharacterRepository characterRepository;

    public MovieController(MovieRepository movieRepository, FranchiseRepository franchiseRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
        this.characterRepository = characterRepository;
    }

    // Create a movie
    @PostMapping("/movie")
    public ResponseEntity<CommonResponse> createMovie(HttpServletRequest request, @RequestBody Movie movie){
        Command cmd = new Command(request);

        movie = movieRepository.save(movie);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = movie;
        commonResponse.message = "New movie with id: " + movie.id;

        HttpStatus response = HttpStatus.CREATED;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read a movie by ID
    @GetMapping("/movie/{id}")
    public ResponseEntity<CommonResponse> getMovieById(HttpServletRequest request, @PathVariable Long id){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(movieRepository.existsById(id)) {
            commonResponse.data = movieRepository.findById(id);
            commonResponse.message = "Movie ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Movie does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read all movies
    @GetMapping("/movie/all")
    public ResponseEntity<CommonResponse> getAllMovies(HttpServletRequest request){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = movieRepository.findAll();
        commonResponse.message = "All movies";

        HttpStatus response = HttpStatus.OK;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Update a movie by ID
    @PatchMapping("/movie/{id}")
    public ResponseEntity<CommonResponse> updateMovie(HttpServletRequest request, @RequestBody Movie updateMovie, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(movieRepository.existsById(id)) {
            Optional<Movie> movieRepositoryById = movieRepository.findById(id);
            Movie movie = movieRepositoryById.get();

            if(updateMovie.title != null) {
                movie.title = updateMovie.title;
            }
            if(updateMovie.genre != null) {
                movie.genre = updateMovie.genre;
            }
            if(updateMovie.releaseYear != 0) {
                movie.releaseYear = updateMovie.releaseYear;
            }
            if(updateMovie.director != null) {
                movie.director = updateMovie.director;
            }
            if(updateMovie.pictureLink != null) {
                movie.pictureLink = updateMovie.pictureLink;
            }
            if(updateMovie.trailerLink != null) {
                movie.trailerLink = updateMovie.trailerLink;
            }

            movieRepository.save(movie);

            commonResponse.data = movie;
            commonResponse.message = "Updated movie with ID: " + movie.id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Movie with ID: " + id + " does not exist.";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Delete a movie
    @DeleteMapping("/movie/{id}")
    public ResponseEntity<CommonResponse> deleteMovie(HttpServletRequest request, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(movieRepository.existsById(id)) {
            Optional<Movie> movie = movieRepository.findById(id);
            Movie theMovie = movie.get();
            for(Character character : theMovie.characters){
                character.movies.remove(theMovie);
            }
            movieRepository.deleteById(id);
            commonResponse.message = "Deleted movie with ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Movie with ID: " + id + " does not exist.";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // get movies in a franchise
    @GetMapping("/movie/franchise/{id}")
    public ResponseEntity<CommonResponse> getMoviesInFranchise(HttpServletRequest request, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(franchiseRepository.existsById(id)) {

            Optional <Franchise> franchise = franchiseRepository.findById(id);

            Franchise franchiseBody = franchise.get();
            List<Movie> movieList = new ArrayList<>(franchiseBody.movies);
            commonResponse.data = movieList;
            commonResponse.message = "Movies for franchise with ID: " + id;

            response = HttpStatus.OK;

        } else {
            commonResponse.data = null;
            commonResponse.message = "Franchise does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Replace list of characters in movie
    @PutMapping("/movie/{id}")
    public ResponseEntity<CommonResponse> replaceCharacter(HttpServletRequest request, @RequestBody List<Long> characters, @PathVariable Long id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(movieRepository.existsById(id)) {
            Optional<Movie> movieRepositoryById = movieRepository.findById(id);
            Movie movie = movieRepositoryById.get();

            movie.characters = new ArrayList<>();
            for(Long character_id : characters) {
                if(characterRepository.existsById(character_id)){
                    Optional<Character> characterOptional = characterRepository.findById(character_id);
                    movie.characters.add(characterOptional.get());
                }
            }

            Movie newMovie = movieRepository.save(movie);
            commonResponse.data = newMovie;
            commonResponse.message = "Replaced movie with ID: " + newMovie.id;
            response = HttpStatus.OK;
        } else {
            commonResponse.message = "Movie not found with ID: " + id;
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }



}
