# Truth About Database

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/noroff-accelerate/java/projects/spring-with-ci/badges/master/pipeline.svg)](https://gitlab.com/LucasPersson/web-api-and-database-with-spring/-/pipelines)

A Hibernate and Spring Boot application in Java. Access and Expose a Database.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Testing](#testing)
- [Assumptions](#assumptions)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project is created for the purpose of creating backend API and endpoints with a database using hibernate and spring.
The assignment was to be completed through pair programing.

## Install

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Usage

For Linux/macOS users, open a terminal and run:

```sh
./gradlew bootRun
```

For Windows users, use `gradlew.bat` instead of `gradlew` in PowerShell.

## Testing

For testing the endpoint of the application you can use the Swagger documentation UI at /swagger-ui/index.html.

## Assumptions

In the assignment we have made the following assumptions.

## Maintainers

[Lucas Persson (@LucasPersson)](https://gitlab.com/LucasPersson), [Christian Neij (@ChristianNeij)](https://gitlab.com/ChristianNeij), [Zahra Ghadban (@zizighadban)](https://gitlab.com/zizighadban)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Lucas Persson, Christian Neij, Zahra Ghadban